var flatiron = require('flatiron'),
fs = require('fs'),
lo = require('lodash'),
nmailer = require('nodemailer'),
smtpTransport = require('nodemailer-smtp-pool'),
nconf = require('nconf'),
juice = require('juice'),
util = require('util'),
winston = require('winston');

var Emailer = function() {
  var self = this;
  var options = options || {};
  
  var templates = {};
  nconf.file('config', {
    file: 'r25ws.json',
    dir: 'config',
    search: true
  });
  mailOptions = nconf.get('email');
  if (options.logger) {
    self.logger = options.logger;
  } else {
    self.logger = new (winston.Logger) ({
      transports: [
    new (winston.transports.Console)()
      ]
    });
  }
//  events.EventEmitter.call(this);

var transporter = nmailer.createTransport(smtpTransport(mailOptions['smtp']));
//smtpTransport(mailOptions['smtp']));

templates['survey'] = fs.readFileSync('tmpl/survey.html');
templates['summary'] = fs.readFileSync('tmpl/summary.html');

self.email = function(emailOptions, callback) {
  var resultStatus = {};
  transporter.sendMail(emailOptions, function(error, info) {
    if(error) {
      resultStatus['status'] = false;
      resultStatus['message'] = error;
    } else {
      resultStatus['status'] = true;
      resultStatus['message'] = info;
    }
    callback(resultStatus);
  });
};

self.emailWithTemplate = function(options, model, tmpl, callback) {
  var tmpl = lo.template(templates[tmpl]);
  var htmlBody = tmpl(model);
  juice.juiceContent(htmlBody, { 'url' : 'http://utsa.edu'}, function(err, body) {
    if (err) {
      status['status'] = false;
      status['message'] = "Failed to juice message";
      callback(status);
    } else {
      options['html'] = body;
      self.email(options, function(status) {
        callback(status);
      });
    } //end else
  }); //end juice
};
}
//  util.inherits(EventList, events.EventEmitter);
module.exports = Emailer;
