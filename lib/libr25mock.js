/*
 * r25ws
 * https://github.com/garza/r25ws
 *
 * Copyright (c) 2014 garza
 * Licensed under the MIT license.
 */

'use strict';

var events = require('events'),
  fs = require('fs'),
  http = require('http'),
  crypto = require('crypto'),
  cookieParse = require('cookie'),
  xml2json = require('xml2json'),
  util = require('util'),
  nconf = require('nconf'),
  winston = require('winston'),
  lo = require('lodash'),
  request = require('request');

var AUTH_BASIC_REALM = "Basic realm=\"R25 WebServices\"";
var libr25ws = function(options) {
  var self = this;
  self.parserOptions = {
      object: true,
      reversible: true,
      coerce: false,
      sanitize: false,
      trim: false
  };  
  options = options || {};
  
  self.init = function(options) {
    //r25ws instances with 25live that are using ldap authentication use a basic realm in the login.xml challenge
    if (options["logger"]) {
      self.logger = options["logger"];
    } else {
      self.logger = new (winston.Logger) ({
        transports: [
          new (winston.transports.Console)()
          //new (winston.transports.File)({ filename: "r25ws.log" })
        ]
      });
    }
  }
  self.init(options);

 self.doRequest = function(options, callback, error) {
   var self = this;
   fs.readFile(options['resource'], function(err, data) {
     if (err) {
       self.logger.info("error while fetching resource: " + options['resource']);
     }
     callback(data.toString());
   });
 };

/*public methods */ 
 self.get = function(options, callback, error) {
   var self = this,
   options = lo.merge(options, self.config);
   options['resource'] = 'mock/multi.xml';
   self.doRequest(options, callback, error);
 }

 self.post = function(options, callback, error) {
   var self = this,
   options = lo.merge(options, self.config),
   originalResource = options["resource"],
   originalData = options["data"];
  
   options.resource = "login.xml";
   delete options.method;
   delete options.data;
   self.login(options, function(success) {
     options["resource"] = originalResource;
     options["data"] = originalData;
     options["method"] = "POST";
     self.doRequest(options, callback, error);
   }, function(failure) {
     error({
       new : "Error while attempting to login",
       message: failure
     });
   });
 }

 self.put = function(options, callback, error) {
   var self = this,
   options = lo.merge(options, self.config),
   originalResource = options["resource"],
   originalData = options["data"];
  
   options.resource = "login.xml";
   delete options.method;
   delete options.data;
   self.login(options, function(success) {
     options["resource"] = originalResource;
     options["data"] = originalData;
     options["method"] = "PUT";
     //console.dir(options);
     self.doRequest(options, callback, error);
   }, function(failure) {
     self.logger.error("get: login returned error");
     error({
       new : "Error while attempting to login",
       message: failure
     });
   });
 }
 
}

module.exports = libr25ws;

