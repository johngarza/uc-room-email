var flatiron = require('flatiron'),
app = flatiron.app,
//fs = require('fs'),
events = require('events'),
lo = require('lodash'),
report = require('../libr25ws'),
util = require('util'),
//nmailer = require('nodemailer'),
//async = require('async'),
nconf = require('nconf'),
xml2json = require('xml2json'),
winston = require('winston'),
moment = require('moment'),
fs = require('fs'),
eventList = require('../eventList');
surveyEmail = require('../surveyEmail');
var Emailer = require('../emailer');

var kSpaceSearchID = '228997';
var kEventSearchURL = 'events.xml?async=T&space_query_id=' + kSpaceSearchID + '&scope=extended&include=customers+attributes+text';
var kEventEditURL = 'event.xml?mode=edit&event_id=';
var kEventEditPUTURL = 'event.xml?return_doc=T&event_id=';

var Survey = function(options) {
  var self = this;
  //events.EventEmitter.call(this);
  options = options || {};
  nconf.file('config', {
    file: 'r25ws.json',
    dir: 'config',
    search: true
  });
  var sMail = new surveyEmail();

  self.parserOptions = {
      object: true,
      reversible: true,
      coerce: false,
      sanitize: false,
      trim: false
  }; 
  self.unmailable = {};
  self.mailable = {};
  self.mailed = {};
  
  if (options.logger) {
    self.logger = options.logger;
  } else {
    self.logger = new (winston.Logger) ({
      transports: [
    new (winston.transports.Console)()
      ]
    });
  }
  
  
  self.listCount = 0;
  self.emailedCount = 0;
  
  self.getDateAttributes = function () {
    var  r25Format  = moment().format('YYYYMMDD');
    var dateAttr = '&start_dt=20140929T124100' + '&end_dt=20141006T124100';
    //var dateAttr = '&start_dt=20141230T050000' + '&end_dt=20150101T180000';
    var firstDay = moment();
    var lastDay = moment();
    
    firstDay.subtract('month', 1).date(1);
    lastDay.date(1).subtract(1, 'days');

    dateAttr = '&start_dt=';
    dateAttr+= firstDay.format('YYYYMMDD') + 'T000000';
    dateAttr+='&end_dt=';
    dateAttr+= lastDay.format('YYYYMMDD') + 'T235959';
    
    
    return dateAttr;
  };

  var dateAttr = self.getDateAttributes();
  // '&start_dt=20141230T050000' + '&end_dt=20150101T180000';
  var getOptions = {
    "resource" : kEventSearchURL + dateAttr
  }
  var eList = new eventList(getOptions);
  
  self.summary = function() {
    var mailOpts = nconf.get('email');
    var summary = mailOpts['summary'];
    var model = { 'mailable' : self.mailable, 'unmailable' : self.unmailable, 'mailed' : self.mailed };
    var em = new Emailer();
    var tmpl = 'summary';
    em.emailWithTemplate(summary, model, tmpl, function(result) {
      if (result['status']) {
        self.logger.info("sent summary email");
      } else {
        self.logger.error("summary email not sent");
        self.logger.error(result);
      }
      self.logger.info("*****************************");
      self.logger.info("ALL EMAILABLE EVENTS HAVE BEEN COMPLETED");
      //self.logger.info("Mailable: " + util.inspect(self.mailable));
      //self.logger.info("Unmailable: " + util.inspect(self.unmailable));
      //self.logger.info("Mailed: " + util.inspect(self.mailed));    
      self.logger.info(self.emailedCount);
      process.exit(0);
    });
  }
  //self.on("emailingComplete", self.summary);

  self.emailProcessed = function(item) {
    //self.logger.info("EMAIL PROCESSED: " + item['locator']);    
    if(lo.isEmpty(self.mailable)) {
      self.summary();
    }
  };
  
  self.startEmail = function(message) {
    self.logger.info("self.listCount: " + self.listCount);
    self.logger.info("message count: " + message['count']);
    lo.forEach(self.mailable, function(item) {
      //self.emit("emailStarted", item);
      var locator = item['locator'];
      /*
      if (lo.indexOf(missingSet, locator) > 0) {
        //self.logger.info("SURVEY: about to email event: " + item['locator'] + ' - ' + item['event']);
        sMail.mail(item, function(resp){
          self.emailedCount++;
          self.mailed[item['locator']] = item;
          delete self.mailable[item['locator']];
          self.emailProcessed(item);
        }, function(err) {
          item['error'] = err;
          self.unmailable[item['locator']] = item;
          delete self.mailable[item['locator']];
          self.emailProcessed(item);
        });

      } else {
        //self.logger.info("SURVEY: skipping event: " + item['locator'] + ' - ' + item['event']);

        item['error'] = 'Skipping';
        //self.unmailable[item['locator']] = item;
        delete self.mailable[item['locator']];
        self.emailProcessed(item);

      }*/
      
      sMail.mail(item, function(resp){
        self.emailedCount++;
        self.mailed[item['locator']] = item;
        delete self.mailable[item['locator']];
        self.emailProcessed(item);
      }, function(err) {
        item['error'] = err;
        self.unmailable[item['locator']] = item;
        delete self.mailable[item['locator']];
        self.emailProcessed(item);
      });
    });
  }
  eList.on("COMPLETE", self.startEmail);

  self.handleSingleEvent = function(event) {
    if (event.email) {
      self.mailable[event['locator']] = event;
    } else {
      self.unmailable[event['locator']] = event;
    }
    self.listCount++;
  };
  eList.on("emitEvent", self.handleSingleEvent);

  //begin processing!
  eList.fetchEvents();
}
//util.inherits(Survey, events.EventEmitter);
module.exports = Survey;
