var flatiron = require('flatiron'),
//app = flatiron.app,
//fs = require('fs'),
events = require('events'),
lo = require('lodash'),
util = require('util'),
//nmailer = require('nodemailer'),
//async = require('async'),
nconf = require('nconf'),
xml2json = require('xml2json'),
winston = require('winston'),
//libr25ws = require('./libr25mock');
libr25ws = require('./libr25ws');


// events.xml?
//space_query_id=228997&
//scope=extended&
//include=attributes+text&
//start_dt=20140901&end_dt=20140903&
//otransform=browse.xsl

var EventList = function(options) {
  var self = this;
  self.r25wsOptions = {};
  options = options || {};
  nconf.file('config', {
    file: 'r25ws.json',
    dir: 'config',
    search: true
  });
  r25wsOptions = nconf.get('r25ws');
  self.parserOptions = {
      object: true,
      reversible: true,
      coerce: false,
      sanitize: false,
      trim: false
  }; 
  self.client = new libr25ws(r25wsOptions);
  
  if (options.logger) {
    self.logger = options.logger;
  } else {
    self.logger = new (winston.Logger) ({
      transports: [
    new (winston.transports.Console)()
      ]
    });
  }
  
  events.EventEmitter.call(this);
  var getOptions = options || {};

  self.reduceRequestor = function(roles) {
    var requestor = {
      name : 'not found',
      email : null
    };
    
    if (lo.isArray(roles)) {
      //multiple events
      lo.forEach(roles, function(item) {
        if (item['r25:role_name']['$t'] == 'Requestor') {
          var contact = item['r25:contact'];
          requestor['name'] = contact['r25:contact_name']['$t'];
          if (lo.has(contact, 'r25:email')) {
            requestor['email'] = contact['r25:email']['$t'];
          }
        }
      });
    } else {
      if (lo.has(roles, 'r25:role_name')) {
        //single object, make sure it's a requestor
        if (roles['r25:role_name'] == 'Requestor') {
          var contact = item['r25:contact'];
          requestor['name'] = contact['r25:contact_name'];
          //self.logger.info(util.inspect(contact));
          if (lo.has(contact, 'r25:email')) {
            requestor['email'] = contact['r25:email'];
          }
        } //else no requestor found
      } //else no roles found
    }
      return requestor;
  }
  
  self.reduceOrgs = function(orgs) {
    var org;
    if (lo.isArray(orgs)) {
      //multiple orgs
      lo.forEach(orgs, function(item) {
        if (item['r25:primary']['$t'] == 'T') {
          org = item['r25:organization_name']['$t'];
        } // else no primary org set
      });
    } else {
      //single org
      if (lo.has(orgs, 'r25:organization_name')) {
        //single org
        org = orgs['r25:organization_name']['$t'];
      } //else no orgs attached
    }
      return org;
  }

  self.reduceEvent = function(event) {
    //self.logger.info(util.inspect(event));
    var roles = event['r25:role'];
    var orgs = event['r25:organization'];
    var requestor = self.reduceRequestor(roles);
    var organization = self.reduceOrgs(orgs);

    var reducedObj = {
      'event' : event['r25:event_name']['$t'],
      'locator' : event['r25:event_locator']['$t'],
      'org' : organization,
      'requestor' : requestor['name'],
      'email' : requestor['email']
    };
    return reducedObj;
  }

  self.checkResults = function(reportObj) {
    var key = reportObj["key"];
    var options = {
      "resource": key + "&wait=5"
    };
    
    self.client.get(options, function(resContent, res) {
      var asyncMessage = null;
      var respJson = {};
      var result = {};
      var emit = false;
      try {
        respJson = xml2json.toJson(resContent, self.parserOptions);
        //self.logger.info("checkResults: JSON PARSE DONE");
        asyncMessage = respJson["r25:results"]["r25:result"]["r25:message"].$t;
      } catch (e) {
        //response is not xml, maybe a pdf? send the data back in a wrapped object
        //self.logger.info("checkResults: " + key + " : CAUGHT");
        result["key"] = key;
        result["data"] = resContent;
        result["type"] = res.headers["content-type"];
        lo.merge(result, reportObj);
        self.emit("checkResultsDone", result);
        emit = true;
      }
      if (asyncMessage) {
        //non null async message, async is not done processing yet
        result["key"] = respJson["r25:results"]["r25:result"]["xl:href"];
        result["message"] = asyncMessage;
        //self.logger.info("checkResults: emit - checkResultsMessage");
        lo.merge(result, reportObj);
        self.emit("checkResultsMessage", result);
      } else {
        if (!emit) {
          //we haven't already reported this data earlier in the catch block
          //content is xml but not a r25:results response, report error? send data back in a wrapped object
          result["key"] = key;
          result["data"] = resContent;
          result["type"] = res.headers["content-type"];
          lo.merge(result, reportObj);
          self.emit("checkResultsDone", result);
        }
      }
    }, function(errorObj) {
      var result = {};
      result["key"] = key;
      result["error"] = errorObj;
      lo.merge(result, reportObj);
      self.emit("checkResultsError", result);
    });
  }

  self.handleResult = function(response) {
    var count = 0;
    var respJson =  {};
    var events = null;
    var summary = {};
    try {
      respJson = xml2json.toJson(response['data'], self.parserOptions);
      events = respJson['r25:events']['r25:event'];
    } catch (e) {
      //response is not xml, maybe a pdf? send the data back in a wrapped object
      //self.logger.info("checkResults: " + key + " : CAUGHT");
      response['error'] = 'Error parsing result document';
      self.emit("checkResultsError", response);
    }
    if (lo.isArray(events)) {
      //multiple events
      lo.forEach(events, function(item) {
        var reducedObj = self.reduceEvent(item);
        self.emit("emitEvent", reducedObj);
        count++
      });
    } else {
      if (lo.has(events, 'r25:event_locator')) {
        //single object, only one event
        count = 1;
        self.emit("emitEvent", self.reduceEvent(events));
      } else {
        count = 0;
        //events is null, no events found
        //noop
      }
    }
    summary['count'] = count;
    summary['size'] = Buffer.byteLength(response['data'], 'utf8');
    summary['message'] = 'Completed enumerating ' + count + ' events.  Total document size: ' + summary['size'] + ' bytes';
    self.emit("COMPLETE", summary);
  };
  
  self.handlerResultMessage = function(message) {
    self.logger.info("async check message: " + message["message"]);
    self.checkResults(message);
  };
  
  self.handlerResultError = function(obj) {
    self.logger.error("ASYNC ERROR - " + obj["key"]);
    self.logger.error(obj["error"]);
  };

  //event handlers
  self.on("checkResultsDone", self.handleResult);
  self.on("checkResultsError", self.handlerResultError);
  self.on("checkResultsMessage", self.handlerResultMessage);

  self.fetchEvents = function() {
    self.client.get(getOptions, function(respObj) {
      //GET success handler
      var asyncKey = '';
      var respJson = {};
      var asyncResponse = '';
      respJson = xml2json.toJson(respObj.toString(), self.parserOptions); //TODO check with live
      //expecting an async result
      try {
        asyncKey = respJson["r25:results"]['r25:result']['xl:href'];
        var asyncObj = {
          'publication_date' : respJson['r25:results']['r25:pubdate'],
          'ws_base_url' : self.r25wsOptions['ws_base_url'],
          'ws_base_uri' : self.r25wsOptions['ws_base_uri'],
          'key' : asyncKey
        };
          self.checkResults(asyncObj);
      } catch(err) {
        //response was not a typical async response, check for results (super fast response?)
        if (lo.has(respJson, 'r25:events')) {
          var result = {};
          result["key"] = 'foo';
          result["data"] = respObj.toString();
          result["type"] = 'text/xml';
          self.emit("checkResultsDone", result);
        }
      }
    }, function(error) {
      //GET error handler
      self.logger.error("Error attempting to create async GET request for event list:");
      self.logger.error(error);
    });
  }
  
}
util.inherits(EventList, events.EventEmitter);
module.exports = EventList;
