/*
 * r25ws
 * https://github.com/garza/r25ws
 *
 * Copyright (c) 2014 garza
 * Licensed under the MIT license.
 */

'use strict';

var events = require('events'),
  fs = require('fs'),
  http = require('http'),
  crypto = require('crypto'),
  cookieParse = require('cookie'),
  xml2json = require('xml2json'),
  util = require('util'),
  nconf = require('nconf'),
  winston = require('winston'),
  lo = require('lodash'),
  request = require('request');

var AUTH_BASIC_REALM = "Basic realm=\"R25 WebServices\"";
var libr25ws = function(options) {
  var self = this;
  self.parserOptions = {
      object: true,
      reversible: true,
      coerce: false,
      sanitize: false,
      trim: false
  };  
  options = options || {};
  
  self.init = function(options) {
    //r25ws instances with 25live that are using ldap authentication use a basic realm in the login.xml challenge
    if (options["logger"]) {
      self.logger = options["logger"];
    } else {
      self.logger = new (winston.Logger) ({
        transports: [
          new (winston.transports.Console)()
          //new (winston.transports.File)({ filename: "r25ws.log" })
        ]
      });
    }
    
    if (options["r25ws"]) {
      if (lo.isString(options["r25ws"])) {
        nconf.file('config', {
          file: options["r25ws"],
          dir: 'config',
          search: true
        });
      }
      if (lo.isPlainObject(options["r25ws"])) {
        nconf.file('config', {
          file: 'r25ws.json',
          dir: 'config',
          search: true
        });
        nconf.set("r25ws", options["r25ws"]);
      }
    } else {
      nconf.file('config', {
        file: 'r25ws.json',
        dir: 'config',
        search: true
      });
    }
    self.config = nconf.get('r25ws');
    //sanity checking, need the following to continue!
    if (!self.config["ws_base_url"]) {
      self.logger.error("Invalid/Missing Parameters: ws_base_url");
      throw { 
        name : "Invalid/Missing Parameter(s)", 
        message : "ws_base_url not set, please pass it in with the options or set in your config.json file"
      };
    }
    if (!self.config["ws_base_uri"]) {
      self.logger.error("Invalid/Missing Parameters: ws_base_uri");
      throw { 
        name : "Invalid/Missing Parameter(s)", 
        message : "ws_base_uri not set, please pass it in with the options or set in your config.json file"
      };
    }
    if (!self.config["username"]) {
      self.logger.error("Invalid/Missing Parameters: username");
      throw { 
        name : "Invalid/Missing Parameter(s)", 
        message : "username not set, please pass it in with the options or set in your config.json file"
      };
    }
    if (!self.config["password"]) {
      self.logger.error("Invalid/Missing Parameters: password");
      throw { 
        name : "Invalid/Missing Parameter(s)", 
        message : "password not set, please pass it in with the options or set in your config.json file"
      };
    }
    //sane defaults for missing optional parameters
    if (!self.config["port"]) {
      self.config["port"] = 80;
    }
    if (!self.config["25live_base_url"] && self.config["ws_base_url"]) {
      self.config["25live_base_url"] = self.config["ws_base_url"];
    }
    if (!self.config["r25live_base_uri"] && self.config["ws_base_url"]) {
      self.config["25live_base_uri"] = "/25live/";
    }
  }
  self.init(options);

 self.handleChallenge = function (options, challengeContent, callback, error) {
   var self = this,
   challenge = '',
   pubdate = '',
   contentJSON = {},
   response = '',
   responseKey = '',
   responseModel = {},
   responseContent = '',
   doc = null,
   postOptions = {},
   postResponse = '',
   postHeaders = {},
   postRequest = null,
   challengeObj = xml2json.toJson(challengeContent, self.parserOptions);
  
   //console.log(options);
   var md5sum = crypto.createHash('md5');
   var  passwordHash = md5sum.update(options["password"]).digest('hex');
   var sessionResponseTemplateFile = fs.readFileSync('lib/login_challenge.tmpl', "UTF-8");
   var sessionResponseTemplate = lo.template(sessionResponseTemplateFile);

   //contentJSON = xml2json.toJson(challengeContent, parserOptions);
   challenge = challengeObj["r25:login_challenge"]["r25:login"]["r25:challenge"].$t;
   pubdate = challengeObj["r25:login_challenge"]["pubdate"];
   responseModel["pubdate"] = pubdate;
   responseModel["username"] = options["username"];
  
   //handle regular stand-alone R25WS as well as R25WS with 25Live and LDAP
   if (challenge == AUTH_BASIC_REALM) {
     self.logger.info("This is a basic authentication challenge");
     response = options["username"] +  ":" + options["password"];
     responseKey = new Buffer(response).toString('base64');
     responseKey = "Basic " + responseKey;
   } else {
     self.logger.info("This is not a basic authentication challenge");
     response = passwordHash + ":" + challenge;
     md5sum = crypto.createHash('md5');
     responseKey = md5sum.update(response).digest('hex');
   } 
   //create our response xml document to the challenge
   responseModel["responseKey"] = responseKey;
   responseContent = sessionResponseTemplate(responseModel);
  
   //console.log("CHALLENGE RESPONSE:");
   //console.dir(options.session);
  
   options["data"] = responseContent;
   self.doRequest(options, function(successObj, postResponse) {
     var postSuccess = xml2json.toJson(successObj, self.parserOptions);
    
     delete options.data;
     if (postResponse.headers["content-type"] == 'text/xml;charset=UTF-8') {
       if (postSuccess["r25:login_response"]) {
         options.session["success"] = postSuccess["r25:login_response"]["r25:login"]["r25:success"].$t == 'T' ? true : false;
         if (options.session["success"]) {
           options.session["security_group_name"] = postSuccess["r25:login_response"]["r25:login"]["r25:security_group_name"].$t;
           options.session["security_group_id"] = postSuccess["r25:login_response"]["r25:login"]["r25:security_group_id"].$t;
           options.session["user_id"] = postSuccess["r25:login_response"]["r25:login"]["r25:user_id"].$t;
           callback(options.session);
         } else {
           //invalid session, failed to login
           self.logger.error("handleChallenge: login failed, data returned indicated no success");
           error({
             new : "Login failed, please review your credentials or configuration file",
             message: postSuccess
           });
         } //end if options.session["success"]
       } else {
         self.logger.error("handleChallenge: post request to login.xml did not contain a login_response, please investigate");
         error({
           new: "Response to login.xml challenge responded with failure",
           message: postSuccess
         });
       } //end if postSuccess["r25:login_response"]
     } else {
       self.logger.error("handleChallenge: POST response was not text/xml, please investigate");
       error({
         new: "Invalid server response from login.xml POST, expected xml, returned content is: " + resp,
         message: postSuccess
       });
     }
   }, function(postError) {
     delete options.data;
     self.logger.error("handleChallenge: doRequest responded with error");
     //console.dir(postError);
     error({
       new: "Challenge response POST failed",
       message: postError
     });
   }); //end doRequest
 };

 self.checkSession = function(options, callback, error) {
   var self = this;

   //logger.info("checkSession begin:");
   delete options.data;
   if (options.session) {
     //logger.info("options.session exists");
     if (options.session.success) {
       //logger.info("opttions.session.success exists");
       options["resource"] = "login.xml";
       options["method"] = "GET";
       //logger.info("checkSession: validating session, doRequest: " + options["method"] + " : " + options["resource"]);
       self.doRequest(options, function(successObj, sessionResponse) {
         var sessionSuccess = xml2json.toJson(successObj, self.parserOptions);
         var loginStatus = 'F';
         var loginMessage = '';
         //logger.info("checkSession: response callback")
         //["r25:login_challenge"]["r25:login"] if not authenticated
         //["r25:login_response"]["r25:login"] if authenticated
         try {
           loginStatus = sessionSuccess["r25:login_response"]["r25:login"]["r25:success"].$t;
           loginMessage = sessionSuccess["r25:login_response"]["r25:login"]["r25:message"].$t;
           //logger.info("checkSession: finished try block");
         } catch (invalidSessionError) {
           self.logger.error("checkSession: caught invalidSessionError");
           self.logger.error("Invalid session found: " + options.session.session_id);
           self.logger.error(JSON.stringify(sessionSuccess));
         }
         if (loginStatus == 'T') {
           //logger.info("checkSession: login_response: " + loginMessage);
           callback(true);
         } else {
           self.logger.info("checkSession:  login_response missing success, invalidating session");
           error(false);
         }
         //logger.info("checkSession: DONE");
       }, function(sesssionError) {
         self.logger.error("there was an error while attempting to validate session");
         error(false);
       });
     } else {
       self.logger.error("checkSession: no options.session.success found");
       error(false);
     } //end if options.session.success
   } else {
     self.logger.error("checkSession: no options.session found");
     //no options.session
     error(false);
   } //end if options.session
 }

 self.grabSessionFromCookie = function (cookieArray) {
   var cookieStr = cookieArray.join(' ');
   var cookies = cookieParse.parse(cookieStr);
   return cookies["JSESSIONID"];
 }

 self.doRequest = function(options, callback, error) {
   var self = this, 
     reqHeaders = {},
     reqOptions = {},
     currentSession = options.session || {},
     reqResponse = '',
     reqRequest = null,
     reqContentJSON = {};
     
     //self.logger.info("doRequest START:");
     //self.logger.info(options);
   reqHeaders['content-type'] = "text/xml;charset=UTF-8";
   if (currentSession["cookies"]) {
     reqHeaders["cookie"] = currentSession["cookies"];
   }
   if (options.data) {
     reqHeaders["Content-Length"]  = Buffer.byteLength(options.data);
   }
   reqOptions["headers"] = reqHeaders;
   reqOptions["hostname"] = options["ws_base_url"];
   reqOptions["path"] = options["ws_base_uri"] + options["resource"];
   reqOptions["port"] = options["port"];
   reqOptions["method"] = options["method"];
   reqOptions["agent"] = false;
  
   self.logger.info("doRequest: reqOptions: " + reqOptions["method"] + " : " + reqOptions["hostname"] + reqOptions["path"]);
   reqRequest = http.request(reqOptions, function(res) {
     res.on('data', function(chunk){
       //logger.info("doRequest: res.on data...");
       reqResponse = reqResponse + chunk;
     });
     res.on('end', function() {
       //self.logger.info("doRequest RESPONSE:");
       //self.logger.info(res["headers"]);
       //self.logger.info(reqResponse);
       
       //make sure it's xml
       //parse it to json
       //logger.info("doRequest: response valid callback: " + res.statusCode);
       if (!((res.statusCode == 200) || (res.statusCode == 201) || (res.statusCode == 202))) {
         //logger.error("doRequest: invalid status code recieved: " + res.statusCode);
         error({
           new: "Invalid request response received",
           message: reqResponse,
           response: res
         });
       } else {
         callback(reqResponse, res);
       }
     });
     res.on('error', function(err) {
       //logger.error("doRequest: http.request responded with error: " + reqOptions.resource + " [" + reqOptions.method + "]");
       error({
         new:"There was an error handling your request.",
         message: err,
         response: res,
         options: options
       });
     });
   });
  
   if (options.data) {
     reqRequest.end(options.data);
   } else {
     reqRequest.end();
   }
 };

/*public methods */ 
 self.get = function(options, callback, error) {
   var self = this,
   options = lo.merge(options, self.config),
   originalResource = options["resource"];
  
   options.resource = "login.xml";
   delete options.method;
   delete options.data;
   self.login(options, function(success) {
     options["resource"] = originalResource;
     options["method"] = "GET";
     self.doRequest(options, callback, error);
   }, function(failure) {
     self.logger.error("get: login returned error");
     error({
       new : "Error while attempting to login",
       message: failure
     });
   });
 }

 self.post = function(options, callback, error) {
   var self = this,
   options = lo.merge(options, self.config),
   originalResource = options["resource"],
   originalData = options["data"];
  
   options.resource = "login.xml";
   delete options.method;
   delete options.data;
   self.login(options, function(success) {
     options["resource"] = originalResource;
     options["data"] = originalData;
     options["method"] = "POST";
     self.doRequest(options, callback, error);
   }, function(failure) {
     error({
       new : "Error while attempting to login",
       message: failure
     });
   });
 }

 self.put = function(options, callback, error) {
   var self = this,
   options = lo.merge(options, self.config),
   originalResource = options["resource"],
   originalData = options["data"];
  
   options.resource = "login.xml";
   delete options.method;
   delete options.data;
   self.login(options, function(success) {
     options["resource"] = originalResource;
     options["data"] = originalData;
     options["method"] = "PUT";
     //console.dir(options);
     self.doRequest(options, callback, error);
   }, function(failure) {
     self.logger.error("get: login returned error");
     error({
       new : "Error while attempting to login",
       message: failure
     });
   });
 }
 
 self.login = function(options, callback, error) {
   var self = this,
   headers = {},
   challengeReq = "",
   resp = "",
   responseContentType = "",
   sessionID = "",
   responseJSON = {};
   options.session = options.session || {};
   //logger.info("login: checkSession:");
  
   self.checkSession(options, function(sessionTrue){
     //logger.info("login: checkSession found a valid session");
     callback(options.session);
   }, function(sessionFalse) {
     //remove the passed in session hash structure
     delete options.session;
     options.session = {};
     self.logger.info("login: checkSession returned FALSE, will attempt login");
     //options = lo.merge(self.config, options);
     options["resource"] = "login.xml";
     options["method"] = "GET";
     self.doRequest(options, function(responseData, responseObj) {
       responseContentType = responseObj.headers["content-type"];
       self.logger.info("set-cookie: ");
       //console.dir(responseObj.headers["set-cookie"]);
       sessionID = self.grabSessionFromCookie(responseObj.headers["set-cookie"]);
       self.logger.info("NEW session_id: " + sessionID);
       if (sessionID) {
         options.session["session_id"] = sessionID;
         options.session["session_date"] = new Date();
         options.session["cookies"] = responseObj.headers["set-cookie"];
         //we asked for json, if we're getting back xml, there must've been an error
         //R25WS always returns errors with xml (unless it's higher, like an apache proxy error)
         if (responseContentType == 'text/xml;charset=UTF-8') {
             //let's not go too far down the rabbithole
             //we have a proper challenge response to our login.xml request
             //let's let handleChallenge finish the handshake
             options["method"] = "POST";
             self.handleChallenge(options, responseData, function(sessionSuccess, sessionObj){
               self.logger.info("CHALLENGE CALLBACK SESSION SUCCESS");
               //console.dir(sessionSuccess);
               self.config.session = sessionSuccess;
               //logger.error("about to SAVE self.config:");
               //console.dir(self.config);
               nconf.set("r25ws", self.config);
               nconf.save(function(saveError) {
                 if (!saveError) {
                   self.logger.info("SAVED session");
                   callback(sessionSuccess);
                 } else {
                   self.logger.error("login: error while attempting to save r25ws config with nconf");
                   error({
                     new: "Error while saving session data to config file",
                     message: saveError
                   });
                 }
               });
             }, function(challengeError) {
               self.logger.error("login: handleChallenge returned error");
               error(challengeError);
             });
           } else {
             self.logger.error("login: response from new session create GET was not text/xml");
             error({
               new: "Content type from login.xml GET was not xml.",
               content: responseData
             });
           } //end if responseContentType
       } else {
         //else no session id, report the error
         self.logger.error("Session ID not found in new session attempt GET request to " + options.resource);
         error({
           new: "No JSESSIONID sent in the server response headers, please investigate",
           message: responseObj.headers
         });
       }
     }, function(err) {
       self.logger.error("login: doRequest returned error: " + options.resource + " [" + options.method + "]");
       error({
         new: "There was an error requesting login.xml",
         message: err,
         options: options
       });
     }); //end doRequest
   }); //end checkSession
 };
}

module.exports = libr25ws;

