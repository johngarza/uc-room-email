/*
 * surveyEmail
 * https://bitbucket.org/johngarza/uc-room-email
 *
 * Copyright (c) 2014 garza
 * Licensed under the MIT license.
 */

'use strict';

var events = require('events'),
fs = require('fs'),
util = require('util'),
nconf = require('nconf'),
winston = require('winston'),
libr25ws = require('./libr25ws'),
http = require('http');

var Emailer = require('../lib/emailer');

var SurveyEmail = function(options) {
  var self = this;
  var tmpl = 'survey';
  var em = new Emailer();
  var r25wsOptions = {};
  var options = options || {};
  nconf.file('config', {
    file: 'r25ws.json',
    dir: 'config',
    search: true
  });
  if (options.logger) {
    self.logger = options.logger;
  } else {
    self.logger = new (winston.Logger) ({
      transports: [
    new (winston.transports.Console)()
      ]
    });
  }
  if (options.r25ws) {
    if (lo.isString(options.r25ws)) {
      nconf.file('config', {
        file: options.r25ws,
        dir: 'config',
        search: true
      });
      r25wsOptions = nconf.get('r25ws');
    }
    if (lo.isPlainObject(options.r25ws)) {
      r25wsOptions = options.r25ws;
    }
  } else {
    //no options.r25ws exists
    nconf.file('config', {
      file: 'r25ws.json',
      dir: 'config',
      search: true
    });
    r25wsOptions = nconf.get('r25ws');
  }
  if (self.logger) {
r25wsOptions.logger = self.logger;
  }
  self.client = new libr25ws(options);
  
  //events.EventEmitter.call(this);
  self.parserOptions = {
      object: true,
      reversible: true,
      coerce: false,
      sanitize: false,
      trim: false
  };  
  self.errors = [];
  
  self.updateEvent = function(contact, success, error) {
    var eventEditURI = 'event.xml?mode=edit&include=attributes&locator=';
    eventEditURL += contact['locator'];
    
    var edited = false;
    
    if (edited) {
      console.log('successfully emailed survey, time to edit the event');
      success(contact);
    } else {
      contact['status'] = false;
      contact['message'] = 'Emailed contact but failed to update event in R25.';
      //console.log(util.inspect(contact));
      error(contact);
    }
  }
  
  self.mail = function(contact, success, error) {
    var sent = false;
    contact['status'] = sent;
    var surveyOptions = {};
    surveyOptions['to'] = contact['email'];
    //surveyOptions['to'] = 'john.garza@utsa.edu';
    surveyOptions['from'] = 'EMCSEvents <emcsevents@utsa.edu>';
    surveyOptions['subject'] = 'UC Feedback Request (' + contact['locator'] + ')';
    contact['url'] = 'http://calendar.utsa.edu/feedback/#prefill/' + contact['locator'];
    
    //var em = new Emailer();
    em.emailWithTemplate(surveyOptions, contact, tmpl, function(result){
      contact['status'] = result['status'];
      if (contact['status']) {
        success(contact);
        //self.updateEvent(contact, success, error);
      } else {
        //console.log(util.inspect(contact));
        contact['message'] = result['message'];
        error(contact);
      }
    });
    if (sent) {
      success(contact);
    } else {
    }
  };
 }

//util.inherits(SurveyEmail, events.EventEmitter);
module.exports = SurveyEmail;