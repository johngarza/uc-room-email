'use strict';

var Emailer = require('../lib/emailer');
var util = require('util');
var emailOpts = {
      'from' : 'John David <john.garza@utsa.edu>',
      'to' : 'john.garza@utsa.edu',
      'subject': 'THIS IS A TEST',
      'html': '<html><body><h1>THIS IS A TEST</h1><p>Successful email sent</p></body></html>',
      'text': 'THIS IS A TEST - Successful email sent'
    };

module.exports = {
  setUp: function(done) {
    done();
  },
  /*
  'emEmail tests':function(test) {
    var em = new Emailer();
    em.email(emailOpts, function(result) {
      if (result.status) {
        test.ok(true, "emEmail tests: Sent email with correct options");
      } else {
        console.log(util.inspect(result));
        test.ok(false, "emEmail tests: Failed to send email with correct options");
      }
      test.done();
    });
  },
  'emEmailFail tests':function(test) {
    var em = new Emailer();
    em.email({}, function(result) {
      if (result.status) {
        test.ok(false, "emEmail tests: Sent email when we should've failed");
      } else {
        test.ok(true, "emEmail tests: Failed to send email with bad options");
      }
      test.done();
    });
  },
  */
  'tmplEmail tests': function(test) {
    var em = new Emailer();
    var template = 'tmpl/summary.html';
    var model = {
      mailed : {},
      unmailable : {}
    };
    em.emailWithTemplate(emailOpts, model, template, function(result) {
      if (result['status']) {
        test.ok(true, 'Sent templated email!');
      } else {
        test.ok(false, 'Failed to send templated email!');
      }
      test.done();
    });
  }
};